package com.ddzsoft.hellomoon.model;

import android.content.Context;
import android.media.MediaPlayer;

import com.ddzsoft.hellomoon.R;

/**
 * Created by Frédéric on 15/07/2014.
 */
public class AudioPlayer{

    private MediaPlayer mPlayer;

    public void stop(){
        if(mPlayer != null){
/*          Release the resource associated with this MediaPlayer, here the wav file, as we don't need it anymore.
            Destroys the instance!*/
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void play(Context c){

        if(mPlayer == null) {

            /* The MediaPlayer object created using "new" is in the "Idle" state, while
            those created with one of the convenient "create" methods are NOT in the "Idle" state.
            The objects are in "Prepared" state if the creation using "create" method is successful */
            mPlayer = MediaPlayer.create(c, R.raw.one_small_step);
        }

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stop();
            }
        });

/*        A MediaPlayer must first enter "Prepared" state before playback can be started.
        Calling start() has no effect on a MediaPlayer object that is already in the "Started" state*/
        mPlayer.start();

/*        The MediaPlayer is then in the "Started" state.
        isPlaying() can be called to test whether the MediaPlayer object is in the "Started" state*/


    }

    public void pause(){
//        Calling pause() has no effect on a MediaPlayer object that is already in the "Paused" state
         mPlayer.pause();
    }

}
