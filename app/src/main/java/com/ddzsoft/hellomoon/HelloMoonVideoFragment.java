package com.ddzsoft.hellomoon;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import com.ddzsoft.hellomoon.model.AudioPlayer;
import com.ddzsoft.hellomoon.model.VideoPlayer;

/**
 * Created by Frédéric on 15/07/2014.
 */
public class HelloMoonVideoFragment extends Fragment {

    private VideoView mVideoView;
    private MediaController mVideoController;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.video_fragment_hello_moon,container, false);

//        TEST COMMIT - FAKE COMMENT
        mVideoView = (VideoView) v.findViewById(R.id.hellomoon_videoView);

//        DOESN'T WORK WITH THE MPG FORMAT
        Uri resourceUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.apollo_17_stroll_mp4);
        Uri resourceUri2 = Uri.parse("android.resource://" + getActivity().getPackageName() + "/raw/apollo_17_stroll_mp4");
        mVideoView.setVideoURI(resourceUri2);

        mVideoController = new MediaController(getActivity());

/*        Designates the view to which the controller is to be anchored.
        Controls the location of the controls on the screen*/
        mVideoController.setAnchorView(mVideoView);

/*        Designates a MediaController instance allowing playback controls to be displayed to the user*/
        mVideoView.setMediaController(mVideoController);

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
/*        The MediaPlayer can continue playing after the HelloMoonAudioFragment is destroyed!
        It works on a different thread*/

    }
}
